use strict;
use warnings;

my @datasets = <*.nex>;


open (OUT, ">paupblock_remove_assumetions.txt") || die "cannot open OUT";

print OUT "#nexus\n";
print OUT "begin paup;\n\n";
print OUT "log file = REMOVE_ASSUPTIONS.LOG.out;\n";
print OUT "set warnreset = no warntsave = no;\n";

foreach my $key (@datasets)
{
    $key =~ s/\.nex//; 
    my $nexusfilename = $key . ".nex";
    my $nexusfileOUT = $key . "no_assumptions.nex1";

    print OUT "execute  $nexusfilename;\n"; 
    print OUT "export file = $nexusfileOUT format = nexus interleaved=no charsPerLine = all;\n";
}
print OUT "quit;\n";
exit;
