# Note script go through a series of log files from paup and calculate HER (Archie 1989).  Her changes slightly whether uninformative characters are excluded or included.  Paup analyses need to include or exclude uninf.  The script here assume characters have been excluded. 
use strict;
use warnings;

my @logs_to_loop = <*.log>;
my @lenghts;
my @number_of_replicates;  
my @characters_teoretical_minimums;
my $excluded_characters; # Parsimony Uninformative characters that have been excluded (in paup)
my $included_characters; #characters in in dataset (parsimony informative)  
my $number_of_taxa;
my $unpermuted_data_tree_lenght;
my $ptp;
my $lenght_tree_from_hs;
my $ConIn;
my $RetIn;
my $ResConIn;
my $HomIn;
my $dataset_name;


#Set up a warnings outfile and initiate it.
open (OUT2, ">>HER_CALCULATIONS_WARNINGS.LOG.txt") || die "cannot open HER_CALCULATIONS_WARNINGS.log\n";
print OUT2 "For the following datasets the PTP unpermuted lenght was shorter than the estimated MPT.  Better trees exists and MPT needs recalculation (HER, CI, RI, TL, HI, RCI unreliable).\n";
####### Done

#Loop through all logfiles to estimate HER / main block of script
foreach my $logfile (@logs_to_loop)
{
    my  $seed_name= $logfile;
    $seed_name=~ s/no_assumptions_HER_NEEDED_INFO\.log//;
    my $outfile= $seed_name . ".HER";
    my $dataset_name = $seed_name . ".nex";
    open (IN, "<$logfile") || die "cannot open $logfile\n";
    open (OUT, ">$outfile") || die "cannot open $outfile\n";
    
    print "Calculating HER for dataset " . $dataset_name . "\n"; 

# extract from a PTP & a Describe tree log output (paup) the tree lenghts of a specified (in paup) number of  randome trees - the lenght of unpermuted data, PTP probability values and  minimal theoretical tree lenght of each character

    my $boul=0;
    my $boul1=0;
    while (<IN>)
    {
	my $line;
	$line = $_;
	chomp $line;
	
	if ($line =~ /Data matrix has /)
	{
	    my @local_array = split (/ /, $line);
	    $number_of_taxa =  $local_array[3];
	    $number_of_taxa =~ s/ //;
	    next;
	}
	if ($line =~ /Total number of characters now excluded/)
	{
	    my @local_array = split (/=/, $line);
	    $excluded_characters = $local_array[1];
	    $excluded_characters =~ s/ //;
	    next;
	}
	if ($line =~ /Number of included characters/)
	{
	    my @local_array = split (/=/, $line);
	    $included_characters = $local_array[1];
	    $included_characters =~ s/ //;
	    next;
	}
	if ($line =~ /Tree length =/)
        {
	    my @local_array = split (/=/, $line);
	    $lenght_tree_from_hs = $local_array[1];
            $lenght_tree_from_hs =~ s/ //;
            next;
	}
	if ($line =~ /Consistency index /)
        {
            my @local_array = split (/=/, $line);
            $ConIn = $local_array[1];
            $ConIn =~ s/ //;
            next;
        }
	if ($line =~ /Homoplasy index /)
	{
	    my @local_array = split (/=/, $line);
	    $HomIn = $local_array[1];
	    $HomIn =~ s/ //;
	    next;
	}
	if ($line =~ /Retention index /)
        {
            my @local_array = split (/=/, $line);
            $RetIn = $local_array[1];
            $RetIn =~ s/ //;
            next;
        }
	if ($line =~ /Rescaled consistency index /)
	{
	    my @local_array = split (/=/, $line);
	    $ResConIn = $local_array[1];
	    $ResConIn =~ s/ //;
	    next;
	}
	if (!$boul && $line =~ / -------------------------/)
	{
	    $boul=1;
	    next;
	}
	if ($boul && $line =~ /length for original/)
	{
	    $boul=0;
	    next;
	}
	if ($boul && $line !~ /length for original/)
	{
	    my @local_array = split (/ +/, $line);
	    if ($local_array[1] =~ /\*/)
	    {
		$local_array[1] =~ s/\*//;
		$unpermuted_data_tree_lenght = $local_array[1];
	    }
	    push (@lenghts, $local_array[1]);
	    push (@number_of_replicates, $local_array[2]);
#	    print "DEBUG AT_LOCAL_1=$local_array[1] AT_LOCAL_2=$local_array[2]\n";
	    next;
	}
	if ($line =~ / P = /)
	{
	    my @local_array2 = split (/ /, $line);
#	    print "DEBUG LOCAL_ARRAY_2 = 0= $local_array2[0] 1= $local_array2[1] 2= $local_array2[2] 3= $local_array2[3] 4= $local_array2[4] 5= $local_array2[5]\n";
	    $ptp = $local_array2[4];
	}

	if (!$boul1 && $line =~ /--------------------------------------------------------------------/)
        {
            $boul1=1;
            next;
        }
	if ($boul1 && $line !~ /paup> quit/)
        {
            my  @local = split (/ +/, $line);

            push (@characters_teoretical_minimums, $local[2]);
#	    print "DEBUG AT_LOCAL_0=$local[0] AT_LOCAL_1=$local[1] AT_LOCAL_2=$local[2] AT_LOCAL_3=$local[3] AT_LOCAL_4=$local[4]\n";                                 
            next;
        }
        if ($boul1 && $line =~ /paup> quit/)
        {
            $boul1=0;
            next;
        }
    }
    close IN;

# print "DEBUG AT_LENGHTS = @lenghts\n";
# print "DEBUG NUMBER OF REPLICATE EACH LENGHT IS = @number_of_replicates\n";
# print "DEBUG CHARS_TEORET_MIN ARE =  @characters_teoretical_minimums\n"; 
#######################################################################################
# DATA PARSING STEP COMPLETED##########################################################
#######################################################################################



#####################################################################################################################
# FROM HERE ON CALCULATIONS NEEDED TO ESTIMATE HER FOLLOWING - Archie (1989) Syst Zool 38:253-269.################### 
#####################################################################################################################


##################### CALCULATION OF THEORETICAL MINIMAL TREE LENGHTS FROM THEORETICA MINIMAL COST OF EACH CHARACTER ON MPT - VALUES FROM PAUP DESCRIBE TREE COMMAND##############
##################### NOTE THIS IS THE SAME TREE USED TO ESTIMATE CI AND RI

    my $theoretical_min_tree_lenght=0;
    foreach my $lengh (@characters_teoretical_minimums)
    {
	$theoretical_min_tree_lenght =  $lengh + $theoretical_min_tree_lenght;
    }

##################################################DONE#############################################################################################################################
######################################################################################################################################################################

   
################### CALCULATION OF THE TOTAL NUMBER OF PERMUTATIONS DONE IN PTP TREES OBTAINED SUMMING TREES FROM EACH BIN IN PTP PAUP OUTPUT.
################### NOTE: THIS IS NEEDED TO CALCULATE AVERAGE LENGHT OF PERMUTED TREE
################### NOTE: THIS IS UNNECESSARY AS NUMBER OP PERMUTATION CAN BE TAKEN FROM PAUP OUTPUT. IT WAS DONE THIS WAY AS A WAY TO DEBUG AS NUMBER OBTAINED HERE IS  
################### EXPETCED TO BE SAME OF NREPS IN PERMUTE COMMAND (IN PAUP).  NOTE: AS PTP INCLUDE ALSO AN ANALYSIS OF THE UNPERMUTED DATASET WHICH WE DO NOT NEED HERE
################### THE TOTAL OF PERMUTED TREES CONSIDERED IS ACTUALLY NREPS -1 . 

    my $total_number_of_binned_trees=0;
    foreach my $bin (@number_of_replicates)
    {
        $total_number_of_binned_trees = $total_number_of_binned_trees + $bin;
    }
    $total_number_of_binned_trees = $total_number_of_binned_trees - 1;

################################################# DONE #####################################################################################################################
############################################################################################################################################################################


################## CALCULATION OF SUM OF ALL PERMUTED TREE LENGHTS. USED TO CALCULATE AVERAGE TREE LENGHT ################################################################## 
################## NOTE: THIS REMOVES THE LENGHT OF THE UNPERMUTED TREE LENGHT ESTIMATED DOING PTP

    my $counter =0;
    my $sum_of_random_tree_lenghts=0;
    foreach my $randomtreelenghts (@lenghts)
    {
	my $element_for_average = $randomtreelenghts * $number_of_replicates[$counter];
	$sum_of_random_tree_lenghts = $sum_of_random_tree_lenghts + $element_for_average;
        $counter++;
    }
    $sum_of_random_tree_lenghts = $sum_of_random_tree_lenghts - $unpermuted_data_tree_lenght;

############################################## DONE #########################################################################################################################
#############################################################################################################################################################################


#########################################################################################################
# HER CALCULATIONS (HER_PTP and HER_MPT) THIS SHOULD CALL A SUBROUTINE IN FUTURE CLEANER VERSIONS########                                                                                
    my $average_random_tree_lenght = $sum_of_random_tree_lenghts / $total_number_of_binned_trees;
    my $MHE = $average_random_tree_lenght - $theoretical_min_tree_lenght;

# HER_PTP CALCULATION (THIS USES MINIMAL TREE LENGHT FROM PTP) ##############################################                                                                               
    my $HE_PTP = $unpermuted_data_tree_lenght - $theoretical_min_tree_lenght;
    my $HER_PTP = 1 - ($HE_PTP/$MHE);

# HER_MPT CALCULATION (THIS USES TREE LENGHT FROM a given MPT) #############################################                                                                                
    my $HE_MPT = $lenght_tree_from_hs - $theoretical_min_tree_lenght;
    my $HER_MPT = 1 - ($HE_MPT/$MHE);

# DONE #####################################################################################################       
############################################################################################################

############################### PRINTING OUTPUT ############################################################

    print OUT "*************************************  Calculation of Homoplasy Excess Ratio  ***********************************************************\n";
    print OUT "*NOTE: two HERS values are calculated. One uses unpermuted lenght from PTP. the other MPT lenght from a given tree MPT (best)           *\n";
    print OUT "*NOTE: scripts worns if PTP unpermuted lenght and MPT unpermuted lenght are different (in most cases they will not be).                 *\n";
    print OUT "*NOTE: when PTP lenght shorter than MPT, estimate MPT not shortest possible and tree search should be re-done. In such cases HER_PTP    *\n";
    print OUT "*NOTE: should be preferred to HER_MPT. Otherwise HER_MPT should be preferred.                                                           *\n";
    print OUT "*NOTE: As in PAUP when PTP is estimated the unpermuted tree lenght is retained in the distribution. Here we have excluded that data     *\n";
    print OUT "*NOTE: when estimating HER values.  Hence Number of Random permutation done to calculate PTP and HER should be equal to the total       *\n";
    print OUT "*NOTE: number of permutations set in paup Permute command, minus one! So, e.g. if you wish to use 100 permuted trees to calculate HER,  *\n";
    print OUT "*NOTE: nreps in permute command in PAUP needs to be set to: nreps = 101.                                                                *\n";
    print OUT "*NOTE: For Murphy et al. study we used only HER_MPT values and tested whether MPT was shorter or equal than best PTP tree.              *\n";                                                     
    print OUT "*****************************************************************************************************************************************\n";
    print OUT "\nDataset                                          = " . $dataset_name . "\n\n";
    print OUT "Parsimony informative (retained) characters      = " . $included_characters . "\n";
    print OUT "Parsimony Uninf + Constant (excluded) characters = " . $excluded_characters . "\n";
    print OUT "Number of Taxa                                   = " . $number_of_taxa . "\n\n";
    print OUT "Number of random permutations                    = " .  $total_number_of_binned_trees . "\n\n";
    print OUT "Lenght of MPT = " . $lenght_tree_from_hs . "\n";
    print OUT "Unpermuted dataset lenght (from PTP)             = " . $unpermuted_data_tree_lenght . "\n";
    
    my $HER_MPT_WARNING = 0;  # This turns on warning when MPT longer than unpermuted PTP
    if ($lenght_tree_from_hs > $unpermuted_data_tree_lenght)
    {
        print OUT "\n!!! WARNING: Lenght unpermuted data in PTP is SHORTER than lenght of MPT                                        !!!\n";
	print OUT "!!! WARNING: HER (calculated using unpermuted PTP lenght) will differ from HER calculated using lenght MPT      !!!\n";
	print OUT "!!! WARNING: As HER should use shortest tree HER_MPT is not reliable. MPT must be reestimated as CI and RI and  !!!\n";
	print OUT "!!! WARNING: other estimates would be equally unreliable.                                                       !!!\n\n";        
	$HER_MPT_WARNING = 1;
    }
    
#   if ($lenght_tree_from_hs < $unpermuted_data_tree_lenght)   ### Uncomment this look if you want to know if PTP Min lenght longer than MPT.  Might be relevant to some uses.
#   {
#	print OUT "\n!!! WARNING: Lenght unpermuted data in PTP is LONGER than lenght of MPT                                      !!!\n";
#	print OUT "!!! WARNING: HER (calculated using unpermuted PTP lenght) will differ from HER calculated using lenght MPT   !!!\n";
#	print OUT "!!! WARNING: THIS is not a problem for HER_MPT but HER_PTP is unreliable and PTP might suggests data set has !!!\n";
#	print OUT "!!! WARNING: less signal than it actually has.  PTP should be repeated.                                      !!!\n\n";
#    }
    print OUT "Theoretical minimal tree lenght                  = " . $theoretical_min_tree_lenght . "\n";
    print OUT "Average lenght of permuted trees                 = " . $average_random_tree_lenght . "\n";
    print OUT "\nPTP                                              = " . $ptp;
    if ($ptp > 0.05)
    {
        print OUT " !!! Attention: dataset is Random according to PTP test !!!\n";
    }
    else {print OUT "\n";}
    print OUT "\nHER_PTP                                          = " . $HER_PTP . "\n";
    print OUT "\nHER_MPT                                          = " . $HER_MPT . "\n\n";
    print OUT "Other measures of potential relevancs (parsed from PAUP)\n";
    print OUT "Consistence Index                                = " . $ConIn . "\n";
    print OUT "Retention Index                                  = " . $RetIn . "\n";
    print OUT "Homoplasy Index                                  = " . $HomIn . "\n";
    print OUT "Rescaled Consistency Index                       = " . $ResConIn . "\n";

    print OUT "********************************** DONE *********************************************************************************************\n";
    close OUT;
    if ($HER_MPT_WARNING)
    {
	$seed_name =~ s/_her_estimation//;
	my $problematic_file = $seed_name . ".nex";
	print OUT2 $problematic_file . "\n";
	$HER_MPT_WARNING = 0;
    }
### Debug alternative WAY TO CALCULATE HER.  $HER and $ALT_HER SHOULD BE THE SAME.
    
# my $ALT_HER_DIVIDEND = $average_random_tree_lenght - $unpermuted_data_tree_lenght;
# my $ALT_HER_DIVISOR = $average_random_tree_lenght - $theoretical_min_tree_lenght;
# my $ALT_HER = $ALT_HER_DIVIDEND / $ALT_HER_DIVISOR;
###  OK tested and they are the same.
    
#print "DEBUG: ALT_HER = $ALT_HER\n";
##### The following is  unnecessary but to stay on safe side and avoid carrying values over all variables are undefined at end of each FOREACH cycle
    undef $seed_name;
    undef $outfile;
    undef $unpermuted_data_tree_lenght;
    undef $ptp;
    undef @lenghts;
    undef @number_of_replicates;
    undef @characters_teoretical_minimums;
    undef $theoretical_min_tree_lenght;
    undef $average_random_tree_lenght;
    undef $HE_PTP;
    undef $HE_MPT;
    undef $HER_PTP;
    undef $HER_MPT;
    undef $MHE;
    undef $sum_of_random_tree_lenghts;
    undef $total_number_of_binned_trees;
    undef $counter;
    undef $boul;
    undef $boul1;
    undef $excluded_characters;
    undef $included_characters;
    undef $lenght_tree_from_hs;
    undef $ConIn;
    undef $HomIn;
    undef $RetIn;
    undef $ResConIn;
    $HER_MPT_WARNING=0;
    undef $number_of_taxa;
    undef $dataset_name;
    print "\n";
}

exit;

