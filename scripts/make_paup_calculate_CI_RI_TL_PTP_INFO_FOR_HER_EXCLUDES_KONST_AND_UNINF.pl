use strict;
use warnings;

my @datasets = <*.nex>;


open (OUT, ">paupblock.txt") || die "cannot open OUT";

print OUT "#nexus\n";
print OUT "begin paup;\n\n";
print OUT "set warnreset = no warntsave = no;\n";

foreach my $key (@datasets)
{
    $key =~ s/\.nex1//; 
    my $nexusfilename = $key . ".nex1";
    my $treefileNewick = $key . "_newick" . ".tree";
    my $logfilename = $key . "_HER_NEEDED_INFO.log";
    print OUT "execute $nexusfilename;\n";
    print OUT "exclude constant uninf;\n";
    print OUT "hs nreps = 10000 addseq=random multrees = off;\n";
    print OUT "savetrees trees = firstOnly file= " . $treefileNewick . " format=Newick;\n";
    print OUT "pscores 1 / CI=yes RI=yes scorefile = " . $key . ".TL_CI_RI_overall_scores;\n";
    print OUT "pscores 1 / single=all TL=no CI=yes RI=no scorefile = " . $key . ".CI_single_character_scores;\n";
    print OUT "pscores 1 / single=all TL=no RI=yes CI=no scorefile = " . $key . ".RI_single_character_scores;\n";
    print OUT "pscores 1 / single=all TL=yes RI=no CI=no scorefile = " . $key . ".TL_single_character_scores;\n\n";

    print OUT "log file = " .  $logfilename . ";\n"; # This is a silly way to do this but work. Not very efficient to open the same file twice.  Nevermind. 
    print OUT "execute " .  $nexusfilename . ";\n";
    print OUT "exclude constant uninf;\n";                                                                                                            
    print OUT "permute nreps = 200001/ nreps = 1 multree = off;\n"; # note HER will be calculated with 200,000 reps but paup uses as one rep the unpermuted data that we will not count, so we need one extra permutation to be done.  200,000 tested looking for when HER converge to a stable values using a test dataset (Zhu et al. 2009a).    
    print OUT "gettrees file = ".  $treefileNewick . ";\n";
    print OUT "deroot;\n";
    print OUT "describetrees 1 / plot=none diagnose=yes;\n";
    print OUT "log stop;\n\n";    
}
print OUT "quit;\n";
exit;
